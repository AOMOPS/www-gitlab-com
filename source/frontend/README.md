# Modern frontend features for www-gitlab-com

Sometimes more modern frontend features for https://about.gitlab.com call
for more modern frontend features.

## Commands

- `yarn run bundle` - Build bundles in this directory
- `yarn run test` - Run tests. These files are located in `/spec/javascripts`
- `yarn run eslint` - Lint javascript files

During development the `yarn run bundle` will be started in a watch mode. This
utilizes middleman's
[external pipelines](https://middlemanapp.com/advanced/external-pipeline/).

## Developing page specific bundles

Let's say you want to add some nice interactive functionality to a handbook page.

1. Create a new file in `frontend/bundles/awesome.js` and add the functionality.
2. Run the bundle commands as defined above.
3. On your handbook page, add the following to the frontend-matter:

   ```
   extra_js:
     - bundles/awesome.js
   ```

4. Done. (Don't forget to create tests)
