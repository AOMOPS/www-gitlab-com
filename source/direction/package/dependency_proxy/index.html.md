---
layout: markdown_page
title: "Category Vision - Dependency Proxy"
---

- TOC
{:toc}

## Dependency Proxy

Many projects depend on a growing number of packages that must be fetched from external sources with each build. This slows down build times and introduces availability issues into the supply chain. In addition, many of these external sources come from unknown and unverified providers, introducing potential security vulnerabilities.

​​For organizations, this presents a critical problem. By providing a mechanism for storing and accessing external packages, we enable faster and more reliable builds. By empowering SecOps to set security policies and remediate known vulnerabilities, we will limit exposure to security issues. By providing a transparent, performant supply chain, we will improve collaboration and drive conversational development for our users.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3ADependency+Proxy)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Target audience and experience

​​This feature impacts five types of users:

-  **Software Developer:** As a developer, you don’t want to spend time worrying about external packages. You want fast build times and a seamless deployment process. The dependency proxy will allow you to store packages for faster and more reliable access. We will start with Docker, our most popular package management tool and expand to include other tools such as NPM and Maven. In the future, we will provide more transparency to help troubleshoot build issues. You will be able to view build logs, track diffs and easily search through the entire proxy.
-  **Security Analyst:** As a security analyst, you need visibility into which external dependencies are being introduced into the supply chain and by whom. The dependency proxy will provide that visibility, but also will grant you the ability to set policies and create lists of approved and banned packages. We know you work on more than one project, so we will give you the tools and ability to discover and navigate all of the proxy repositories at an organizational level. We’ll start with the command line, add support for APIs, and evaluate adding a centralized dashboard.
- **QA Engineer:** As a QA engineer, you need the ability to work across multiple projects and teams seamlessly. The goal of the dependency proxy will be to ensure that each codebase you test not only runs on your machine (or environment) but that you can start testing quickly and get back to automation.
-  **Systems Administrator​​:** As a systems administrator, you need to know that your entire DevOps pipeline runs smoothly and efficiently. For those of you at organizations that limit internet access, the dependency proxy will allow you to control the chaos and manage a single, optimized storage system for all your external packages. We will provide deduplication, garbage collection and the ability to set your own policies to help optimize storage and save money.
- **Compliance Teams:** At GitLab, we strive to create a Platform where everyone can contribute. We know at large organizations that compliance teams have a vested interest in controlling and discovering which packages are used throughout the organization. As we continue to develop features for the dependency proxy, we will provide visibility into the supply chain for compliance teams. This may be reports and alerts or a centralized dashboard depending on future user research.
​​
## Usecases listed

1. Provide a single method of reaching upstream package management utilities, in the event they are not otherwise reachable. This is commonly due to network restrictions. 
1. Let proxy packages act as a cache for increased pipeline build speeds. 
1. Verify package integrity from one single place. See what has been changed and test them for security vulnerabilities (part of black duck model). 
1. Filter the available upstream packages to include only approved, whitelisted packages. 
1. Track which dependencies are utilized by which projects when pulled through the proxy. (Perhaps when authenticated with a `CI_JOB_TOKEN`.
1. Audit logs in order to find out exactly what happened and with what code. 
1. Operate when fully cut off from the internet with local dependencies.
1. Enforce policies at the proxy layer (e.g. scan packages for licenses and only allow packages with compatible licenses).

## What's next & why

We have launched the MVC of the [dependency proxy](https://gitlab.com/gitlab-org/gitlab/issues/7934) with [limited availability.](https://gitlab.com/gitlab-org/gitlab/issues/7934#availability) [gitlab-#78](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/78) will update gitlab.com to use Puma, making the Dependency Proxy available for all public projects. 

[gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/issues/11582), will focus on adding authentication to enable use of the dependency proxy with private projects. [gitlab-#11631](https://gitlab.com/gitlab-org/gitlab/issues/11631), will add the ability to delete items from the Dependency Proxy. 

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Authentication to support private projects](https://gitlab.com/gitlab-org/gitlab/issues/11582)
- [Purge / delete from cache](https://gitlab.com/gitlab-org/gitlab/issues/11631)
- [Add limits to dependency proxy](https://gitlab.com/gitlab-org/gitlab/issues/11637)

## Competitive landscape

* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Docker+Registry#DockerRegistry-RemoteDockerRepositories)
* [Nexus](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-ProxyRepository)

JFrog is the leader in this category. They offer 'remote repositories' which serve as a caching repository for various package manager integrations. Utilizing the command line, API or a user interface, a user may create policies and control caching and proxying behavior. A Docker image may be requested from a remote repository on demand and if no content is available it will be fetched and cached according to the user's policies. In addition, they offer support for many of major packaging formats in use today. For storage optimization, they offer check-sum based storage, deduplication, copying, moving and deletion of files.

​​However, since they have focused on solving all possible usecases, there is room for simplification and design improvements. We believe this will allow GitLab to provide a more accessible and easier-to-navigate solution. In addition, we provide added value by combining this with our own CI/CD services, improving speed and having everything on-premise.
​​
## Top Customer Success/Sales issue(s)

The top customer success issue is [gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/issues/11582), which will introduce authentication and allow users to leverage the Dependency Proxy with private projects. 

## Top user issue(s)

To improve capabilities for our existing users, we want to deliver [gitlab-#9164](https://gitlab.com/gitlab-org/gitlab/issues/9164) (npm) and [gitlab-#9163](https://gitlab.com/gitlab-org/gitlab/issues/9163) (Maven), which will add support for the dependency proxy to the npm and Maven repository.

## Top internal customer issue(s)

Our top internal customer is the Distribution team, which would like to avoid relying on external sources for downloading dependencies. [gitlab-distribution#496](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/496) will deploy [Puma](https://docs.gitlab.com/omnibus/settings/puma.html) to gitlab.com and allow them to begin using the Dependency Proxy for images sourced from [DockerHub](https://hub.docker.com/).

## Top Vision Item(s)

Our top vision item is [gitlab-#11680](https://gitlab.com/gitlab-org/gitlab/issues/11680), which will introduce search and make items in the Dependency Proxy easier to discover. 